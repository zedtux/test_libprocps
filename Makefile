CC=g++
PKGCONFIG=`pkg-config --cflags --libs libprocps`
CFLAGS=-pedantic -Wall -W -g $(PKGCONFIG)
LDFLAGS=$(PKGCONFIG)

OBJ=main.o
EXEC=procinfo

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) -o $(EXEC) $(OBJ) $(LDFLAGS)

%.o: %.cpp
	$(CC) $(CFLAGS) -o $@ -c $<

clean:
	rm -rf $(OBJ) $(EXEC)