#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <proc/readproc.h>

int main (int argc, char* argv[])
{
    proc_t my_process_info;
    proc_t process_info;
    pid_t pid;

    look_up_our_self(&my_process_info);
    std::cout << "This process " << my_process_info.tgid << ":" << std::endl;
    std::cout << "    cmd: " << my_process_info.cmd << std::endl;
    std::cout << "    cmdline[0]: " << my_process_info.cmdline[0] << std::endl;

    if(argc > 1)
    {
        pid = (pid_t) atoi(argv[1]);
        if (get_proc_stats(pid, &process_info))
        {
            std::cout << "Process " << pid << ":" << std::endl;
            std::cout << "    cmd: " << process_info.cmd << std::endl;
            std::cout << "    cmdline: " << process_info.cmdline << std::endl;
        } else {
            std::cerr << "PID " << pid << ": get_proc_stats();" << std::endl;
            std::cerr << "    " << strerror(errno) << std::endl;
        }
    } else {
        std::cout << "Usage: " << argv[0] << " <PID>" << std::endl;
    }

    return 0;
}
