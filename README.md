# test_libprocps

## Purpose
Testing libprocps (with Ubuntu) and having issue with executing `make` command:

    g++ -pedantic -Wall -W -g `pkg-config --cflags --libs libprocps` -o main.o -c main.cpp
    g++ -o procinfo main.o `pkg-config --cflags --libs libprocps`
    main.o: In function `main':
    /home/zedtux/Developments/test_libprocps/main.cpp:22: undefined reference to `get_proc_stats'
    collect2: error: ld returned 1 exit status
    make: *** [procinfo] Error 1

Calling method `look_up_our_self()` is working (linker doesn't complain about it) but using `get_proc_stats()` isn't working.

## Dependencies

With Ubuntu you have to install the package `libprocps0-dev`
